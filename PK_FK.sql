create table supplier 
( supplier_id numeric(10) NOT NULL,
  supplier_name varchar2(100) NOT NULL,
  contact_name varchar2(50)
  CONSTRAINT supplier_pk PRIMARY KEY (supplier_id)
);

create table products
( product_id numeric(10) NOT NULL,
  supplier_id numeric(10) NOT NULL,
  CONSTRAINT fk_supplier
    FOREIGN KEY (supplier_id)
    REFERENCES supplier(supplier_id)
);


create table supplier
( supplier_id numeric(10) NOT NULL,
  supplier_name varchar2(100) NOT NULL,
  contact_name varchar(100),
  CONSTRAINT supplier_pk(supplier_id,supplier_name)
);

create table products
( product_id numeric(10) NOT NULL,
  supplier_id numeric(10) NOT NULL,
  supplier_name varchar2(100) NOT NULL,
  CONSTRAINT fk_supplier_comp
    FOREIGN KEY (supplier_id,supplier_name),
    REFERENCES supplier(supplier_id,supplier_name)
;