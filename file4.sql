create table talabalar(
	id int not null primary key,
	name varchar2(100),
	tugilgan_vaqti date,
	uqishga_qabul_qilingan_yili number(4),
	shartnoam_id number unique,
	address char(200),
	yakuniy_ball binary_float,
	guruh_id int,
	foreign key (guruh_id) references guruh(guruh_id)
);

create table guruh (
	guruh_id int not null primary key,
 	oquvchi_soni number not null,
	fakultet_name varchar2(50)
);